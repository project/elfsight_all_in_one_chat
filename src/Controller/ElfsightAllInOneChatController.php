<?php

namespace Drupal\elfsight_all_in_one_chat\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightAllInOneChatController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/all-in-one-chat/?utm_source=portals&utm_medium=drupal&utm_campaign=all-in-one-chat&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
